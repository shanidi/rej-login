import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';//נחוץ כי רק באמצעותו אפשר להגיכ ליוזר ID
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email:string;
password:string;
  constructor(private authService:AuthService,private router:Router) { }

  login(){
    this.authService.login(this.email,this.password)
    .then(value=>{
      this.router.navigate(['items'])});
    this.email=''
    this.password=''
  }

  

  ngOnInit() {
  }

}
