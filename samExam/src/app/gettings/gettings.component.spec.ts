import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GettingsComponent } from './gettings.component';

describe('GettingsComponent', () => {
  let component: GettingsComponent;
  let fixture: ComponentFixture<GettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
