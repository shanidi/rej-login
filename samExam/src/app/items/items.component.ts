import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
itemss=[];
name;
price;

  constructor(private authServie:AuthService,
  private db:AngularFireDatabase) { }

  ngOnInit() {
    this.authServie.user.subscribe(user=>{
      user.uid
      this.db.list('/user/'+user.uid+'/items/').snapshotChanges().subscribe(
        itemss=>{
          this.itemss=[];
            itemss.forEach(
              item =>{
                let e=item.payload.toJSON();
                e["$key"]=item.key;
                this.itemss.push(e);
                    }
           )
        }
      )
    })
  }

}
