import {MatTableModule} from '@angular/material/table';//מטירייל-טבלה

import { AngularFireModule } from '@angular/fire';//פיירבייס
import { AngularFireDatabaseModule } from '@angular/fire/database';//פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//פיירבייס
import { environment } from '../environments/environment'; //פיירבייס


import { RouterModule, Routes } from '@angular/router';//ראוטר

import {MatCardModule} from '@angular/material/card';//לנאב
import {MatInputModule} from '@angular/material/input';//לטופס התחברות
import{FormsModule} from '@angular/forms'; //לטופס
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';//לטוס- אבל להוסיף תמיד
import {MatDialogModule} from '@angular/material/dialog';//פופאפ
import {MatFormFieldModule} from '@angular/material/form-field';//פופאפ

import {MatButtonModule} from '@angular/material/button';//כפתורים
import { MatGridListModule } from '@angular/material/grid-list';//טבלה

import { MatSelectModule } from '@angular/material/select';//לסינון

import { MatCheckboxModule } from '@angular/material/checkbox';//צק בוקס

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { GettingsComponent } from './gettings/gettings.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GettingsComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,//מובנה
    MatTableModule, //מטירייל- טבלה
    AngularFireModule.initializeApp(environment.firebase), //פיירבייס
    AngularFireDatabaseModule,//פיירבייס
    AngularFireAuthModule,//פיירבייס
    MatCardModule, //לנאב
    MatInputModule,//להתחברות
    FormsModule,//לטופס
    BrowserAnimationsModule, //לטופס אבל להוסיף תמיד
    MatDialogModule,//פופאפ
    MatFormFieldModule,//פופאפ
    MatButtonModule,//כפתורים
    MatGridListModule,
    MatSelectModule,//סינון
    MatCheckboxModule,
   

    RouterModule.forRoot([
      {path:'',component:ItemsComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
      {path:'items',component:ItemsComponent},
      {path:'login',component:LoginComponent},  
      {path:'register',component:RegisterComponent},       
      {path:'gettings',component:GettingsComponent},   
      {path:'register',component:RegisterComponent},     
      {path:'**',component:NotFoundComponent},
      ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
