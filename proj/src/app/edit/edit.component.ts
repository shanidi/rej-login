import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { flush } from '@angular/core/testing';
import { ListService } from '../list.service';
import {MatDialog, MatDialogConfig} from "@angular/material";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { FormGroup } from '@angular/forms';
import { element } from '@angular/core/src/render3';
import { ListComponent } from '../list/list.component';



@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  form: FormGroup;
  name:string;// להוספה 
  color:string;//להוספה
  price:number;//להוספה
  currency:string; //להוספה
  key:string;

  constructor(private listService:ListService, private dialog: MatDialog,private dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) data)
  
  {   this.name = data.name;
      this.color = data.color; 
      this.currency = data.currency;
      this.price=data.price;
      this.key=data.key;
    }

  save(name,color,price,currency) {
    this.listService.updateName(this.key,name);
    this.listService.updateColor(this.key,color);
    this.listService.updatePrice(this.key,price);
    this.listService.updateCurrency(this.key,currency);
    this.dialogRef.close();
}

close() {
    this.dialogRef.close();
}

  ngOnInit() {


  }

    




}
