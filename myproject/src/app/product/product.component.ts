import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() data:any; //דרוש לצורך בנית האלמננט
  name; //דרוש לצורך בנית האלמננט
  color;//דרוש לצורך בנית האלמננט
  currency;//דרוש לצורך בנית האלמננט
  price;//דרוש לצורך בנית האלמננט

  constructor() { }

  ngOnInit() {
    this.name = this.data.name;//דרוש לצורך בנית האלמננט
    this.color = this.data.color;//דרוש לצורך בנית האלמננט
    this.currency = this.data.currency;//דרוש לצורך בנית האלמננט
   this.price = this.data.price;//דרוש לצורך בנית האלמננט
  }

}