import { BrowserModule } from '@angular/platform-browser';//מובנה
import { NgModule } from '@angular/core';//מובנה

import { AppComponent } from './app.component';//מובנה
import { ListComponent } from './list/list.component';//מובנה
import { ProductComponent } from './product/product.component';//מובנה

import {MatTableModule} from '@angular/material/table';//מטירייל-טבלה

import { AngularFireModule } from '@angular/fire';//פיירבייס
import { AngularFireDatabaseModule } from '@angular/fire/database';//פיירבייס
import { AngularFireAuthModule } from '@angular/fire/auth';//פיירבייס
import { environment } from '../environments/environment'; //פיירבייס

import { RouterModule, Routes } from '@angular/router';//ראוטר

import {MatCardModule} from '@angular/material/card';//לנאב

import { NavComponent } from './nav/nav.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';


@NgModule({
  declarations: [
    NavComponent,
    AppComponent,//מובנה
    ListComponent,//מובנה
    ProductComponent, LoginComponent, HomeComponent, RegisterComponent//מובנה

  ],
  imports: [
    BrowserModule,//מובנה
    MatTableModule, //מטירייל- טבלה
    AngularFireModule.initializeApp(environment.firebase), //פיירבייס
    AngularFireDatabaseModule,//פיירבייס
    AngularFireAuthModule,//פיירבייס
    MatCardModule, //לנאב
   

    RouterModule.forRoot([
      {path:'',component:HomeComponent}, //מכיל יו-אר-אל אם הוא ריק מדובר באינדקס בקומפוננט נרשום את הקומפוננטים שמשתנים
      {path:'list',component:ListComponent},
      {path:'home',component:HomeComponent},
      {path:'login',component:LoginComponent},      
      {path:'register',component:RegisterComponent},
      {path:'**',component:HomeComponent},
    
      ]),
  ],
  providers: [],//מובנה
  bootstrap: [AppComponent]//מובנה
})
export class AppModule { }
